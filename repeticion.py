#repetición de números
#autor: Danilo Delgado
#email: edwin.delgado@unl.edu.ec
contador=0
t=0
while True:
    N = input("ingrese un número (o fin para terminar): ")
    if N.lower() in "fin":
        break
    try:
        t = t+int(N)
        contador = contador+1
    except ValueError:
        print("Entrada invalida")
print("proceso terminado")

print("la suma de los número es: ", t)
print("la cantidad de los números introducidos es: ", contador)
print("el promedio de los números ingresados es: ", t / contador)